# Pi4LedControlledByButton

Pi4J example code which toggles LED that is connected in a circuit involving buttons and the LED.

To compile and build:
Cd to the project directory and run the following commands.
          mvn clean package
          sudo target/distribution/run.sh
